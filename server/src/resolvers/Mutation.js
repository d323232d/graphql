function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    text: args.text
  })
};

async function postReview(parent, args, context, info) {
  const messageExist = await context.prisma.$exists.message({
    id: args.messageId
  });

  if (!messageExist) {
    throw new Error(`Message with ID ${args.messageId} doesn't exist`);
  }
  return context.prisma.createReview({
    text: args.text,
    message: { connect: { id: args.messageId } }
  })
};

module.exports = {
  postMessage,
  postReview
}