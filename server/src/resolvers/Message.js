function reviews(parent, args, context) {
  return context.prisma.message({
    id: parent.id
  }).reviews();
}

module.exports = {
  reviews
}