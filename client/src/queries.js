// import { gql } from 'graphql-tag';
import { gql } from '@apollo/client';

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
      count
      messageList {
        id
        createdAt
        text
        likes
        dislikes
        review {
          id
          createdAt
          text
          likes
          dislikes
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      createdAt
      text
      likes
      dislikes
      review {
        id
        createdAt
        text
        likes
        dislikes
      }
    }
  }
`;

export const POST_REVIEW_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postReview(messageId: $messageId, text: $text) {
      id
      text 
    }
  }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      createdAt
      text
      likes
      dislikes
      reviews {
        id
        createdAt
        text
        likes
        dislikes
      }
    }
  }
`;



