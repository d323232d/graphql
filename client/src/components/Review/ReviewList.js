import React from "react";
import ReviewForm from "./ReviewForm";
import ReviewItem from "./ReviewItem";

const ReviewList = props => {
  const [showReviewForm, toggleForm] = React.useState(false);
	const { messageId, reviews } = props;

	return (
		<div className="review-list">
			{reviews.length > 0 && <span className="review-list-title">reviews</span>}

			{reviews.map(item => <ReviewItem key={item.id} {...item}/>)}

			<button className="review-button" onClick={()=> toggleForm(!showReviewForm)}>
				{showReviewForm ? "Close reviews" : "Add reviews"}
			</button>
      
			{ showReviewForm && <ReviewForm
				messageId={messageId}
				toggleForm={toggleForm}
			/>}
		</div>
	)

};



export default ReviewList;