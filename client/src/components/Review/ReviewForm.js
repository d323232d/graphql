import React, { useState } from "react";
import { Mutation } from 'react-apollo';
import { POST_REVIEW_MUTATION, MESSAGE_QUERY } from "../../queries";

const ReviewForm = props => {
  const { messageId, toggleForm } = props;
  const [text, setText] = useState('');

  const _updateStoreAfterAddingReview = (store, newReview, messageId) => {
    const sortedBy = 'createdAt_DESC';

    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        sortedBy
      }
    });

    const reviewMessage = data.messages.messageList.find(
      item => item.id === messageId
    );
    reviewMessage.reviews.push(newReview);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    toggleForm(false);
  }

  return (
    <div className="review-form">
      <div className="input-wrapper">
        <textarea
          onChange={e => setText(e.target.value)}
          placeholder="Your review"
          value={text}
        />
      </div>

      <Mutation
        mutation={POST_REVIEW_MUTATION}
        variables={{ messageId, text }}
        update={(store, { data: { messageReview } }) => {
          _updateStoreAfterAddingReview(store, messageReview, messageId)
        }}
      >
        {postMutation => <button onClick={postMutation}>Review</button>}
      </Mutation>

    </div>

  )

}



export default ReviewForm;