import React from "react";

const ReviewItem = props => {
  return (
    <div className="review-item">
      <span>#{props.id}</span>
      <p>{props.text}</p>
    </div>
  )
};

export default ReviewItem;