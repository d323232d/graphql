import React from "react";
import ReviewList from "../Review/ReviewList";

const MessageItem = props => {

  return (
    <div className='message-item'>
      <span>#{props.id}</span>
      <p>{props.text}</p>

      <ReviewList messageId={props.id} reviews={props.reviews}/>

    </div>

  )

}



export default MessageItem;