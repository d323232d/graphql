
import { Link, Route, Switch } from 'react-router-dom';
import './App.css';
import MessageForm from './components/Message/MessageForm';
import MessageList from './components/Message/MessageList';

function App() {
  return (
    <div className="App">
      <div className="nav-panel">
        <Link to="/">Messages</Link>
        <Link to="/add-message">Add message</Link>
      </div>
      <Switch>
        <Route exact path="/" component={MessageList}/>
        <Route exact path="/add-message" component={MessageForm}/>
      </Switch>
    </div>
  );
}

export default App;
